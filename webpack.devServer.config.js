const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const StyleLintPlugin = require("stylelint-webpack-plugin");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");
const { VueLoaderPlugin } = require("vue-loader");

const path = require("path");

module.exports = {
  /*
		webpack-dev-server will monitor the code dependency
		of these entry points, and re-create the bundle
	  	when changes are detected. In this example, the main
	  	javascript is main.js, and it points to
	  	other code dependencies
	  	!Путь, где лежит исходный JS
  	*/
  entry: "./src/js/main.js",
  /*
		This specifies where javascript bundle is created when
	  	webpack CLI is run. However, webpack-dev-server is only 
	  	concerned with the 'filename' parameter.
	  	webpack-dev-server generates the bundle with the 'filename' in
	  	memory. It never creates an actual file in the 'path' specified
	  	unlike the webpack CLI.
	  	!Путь по которому будет лежать сгенерированный JS
  	*/
  output: {
    filename: "js/bundle-[hash].js"
    //path: path.resolve(__dirname, './build/assets'),
    //DevServer не берет в расчет этот PublicPath
    //publicPath: '/assets/js/',
  },
  devtool: "eval-sourcemap",
  devServer: {
    overlay: true,
    /* 
			Can be omitted unless you are using 'docker'
		*/
    //host: '0.0.0.0',

    /*
			This is where webpack-dev-server serves your bundle
	    	which is created in memory.
	    	To use the in-memory bundle,
	    	your <script> 'src' should point to the bundle
	    	prefixed with the 'publicPath', e.g.:
	    	<script src='http://localhost:9001/assets/bundle.js'></script>
	    	!Корневой путь, по которому DevServer будет генерировать файлы в памяти
    	*/
    publicPath: "/",

    /*
			The local filesystem directory where static html files
	    	should be placed.
	    	Put your main static html page containing the <script> tag
	    	here to enjoy 'live-reloading'
	    	E.g., if 'contentBase' is '../views', you can
	    	put 'index.html' in '../views/main/index.html', and
	    	it will be available at the url:
	    	https://localhost:9001/main/index.html
	    	!Путь где лежит исходный index.html
    	*/
    contentBase: path.join(__dirname, "/src"),

    /*
			'Live-reloading' happens when you make changes to code
	    	dependency pointed to by 'entry' parameter explained earlier.
	    	To make live-reloading happen even when changes are made
	    	to the static html pages in 'contentBase', add 
	    	'watchContentBase'
    	*/
    watchContentBase: true

    //compress: true,
    //port: 8080
  },
  plugins: [
    new MiniCssExtractPlugin({
      hash: true,
      filename: "css/main-[hash].css"
    }),
    new HtmlWebpackPlugin({
      //Путь, где лежит страница в которую вставлять скрипты и css
      //template: "./src/index.html",
      template: "./src/index.pug",
      //Путь, где лежит страница на DevServer
      filename: "index.html",
      inject: true
    }),
    new StyleLintPlugin({
      configFile: ".stylelintrc",
      context: "./src/sass",
      failOnError: false,
      quiet: false,
      syntax: "scss"
    }),
    new SpriteLoaderPlugin(),
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.html$/,
        use: "html-loader"
      },
      {
        test: /\.js$/,
        exclude: "/(node_modules)/",
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env"]
            }
          },
          {
            loader: "eslint-loader"
          }
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: "/(node_modules)/",
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                require("cssnano")({
                  preset: "default"
                })
              ]
            }
          },
          {
            loader: "group-css-media-queries-loader"
          },
          {
            loader: "sass-loader"
          }
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        exclude: [/img/],
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "fonts",
            publicPath: "../fonts"
          }
        }
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        exclude: [/fonts/],
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img",
              publicPath: "../img"
            }
          },
          {
            loader: "image-webpack-loader",
            options: {
              mozjpeg: {
                progressive: true,
                quality: 70
              }
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-sprite-loader",
            options: {
              extract: true,
              spriteFilename: svgPath => `svg/sprites${svgPath.substr(-4)}`
            }
          },
          "svg-fill-loader",
          "svgo-loader"
        ]
      },
      {
        test: /\.pug$/,
        use: ["pug-loader"]
      },
      {
        test: /\.vue$/,
        use: "vue-loader"
      }
    ]
  }
};
