var smartgrid = require("smart-grid");

/* It's principal settings in smart grid project */
var settings = {
  outputStyle: "scss",
  /* less || scss || sass || styl */
  columns: 12,
  /* number of grid columns */
  offset: "30px",
  /* gutter width px || % || rem */
  mobileFirst: true,
  /* mobileFirst ? 'min-width' : 'max-width' */
  container: {
    maxWidth: "1200px",
    /* max-width оn very large screen */
    fields: "30px" /* side fields */
  },
  breakPoints: {
    xl: {
      width: "1440px"
    },
    lg: {
      width: "1024px"
      /* -> @media (max-width: 1100px) */
    },
    md: {
      width: "768px"
    },
    sm: {
      width: "320px"
      /* fields:
        "320px" /* set fields only if you want to change container.fields */
      /* fields:
        "15px" */
    }
    /* 
        We can create any quantity of break points.
 
        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
  }
};

smartgrid("./src/sass", settings);
