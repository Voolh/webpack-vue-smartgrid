const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const StyleLintPlugin = require("stylelint-webpack-plugin");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");
const { VueLoaderPlugin } = require("vue-loader");

const path = require("path");

module.exports = {
  /*
	  	!Путь, где лежит исходный JS
  	*/
  entry: "./src/js/main.js",
  /*
	  	!Путь по которому будет лежать сгенерированный JS
  	*/
  output: {
    filename: "js/bundle.js",
    path: path.resolve(__dirname, "./build")
  },
  plugins: [
    new MiniCssExtractPlugin({
      hash: true,
      filename: "css/main.css"
    }),
    new HtmlWebpackPlugin({
      //Путь, где лежит страница в которую вставлять скрипты и css
      template: "./src/index.pug",
      //Путь, где лежит страница на DevServer
      filename: "index.html",
      inject: true
    }),
    new StyleLintPlugin({
      configFile: ".stylelintrc",
      context: "./src/sass",
      failOnError: false,
      quiet: false,
      syntax: "scss"
    }),
    new CopyWebpackPlugin([
      {
        from: "./src/img",
        to: "img",
        ignore: ["._*"]
      }
    ]),
    new SpriteLoaderPlugin(),
    new CleanWebpackPlugin({
      verbose: true,
      dry: false
    }),
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: true,
              removeComments: true,
              collapseWhitespace: true,
              //Оставляем атрибуты для парсинга пустым массивом, чтобы исключить
              //обработку <img src="">. Файлы в этом случае копируются через CopyWebpackPlugin
              attrs: []
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: "/(node_modules)/",
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env"]
            }
          },
          {
            loader: "eslint-loader"
          }
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: "/(node_modules)/",
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader",
            options: {
              url: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                require("cssnano")({
                  preset: "default"
                }),
                require("autoprefixer")({
                  browsers: ["last 2 versions"]
                })
              ]
            }
          },
          {
            loader: "group-css-media-queries-loader"
          },
          {
            loader: "sass-loader"
          }
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        exclude: [/img/],
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "fonts",
            publicPath: "../fonts"
          }
        }
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        exclude: [/fonts/],
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img",
              publicPath: "../img"
            }
          },
          {
            loader: "image-webpack-loader",
            options: {
              mozjpeg: {
                progressive: true,
                quality: 70
              }
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-sprite-loader",
            options: {
              extract: true,
              spriteFilename: svgPath => `svg/sprites${svgPath.substr(-4)}`
            }
          },
          "svg-fill-loader",
          "svgo-loader"
        ]
      },
      {
        test: /\.pug$/,
        use: ["pug-loader"]
      },
      {
        test: /\.vue$/,
        use: "vue-loader"
      }
    ]
  }
};
