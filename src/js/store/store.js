import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: null
  },
  actions: {
    ACTION_LOAD_DATA: (context, payload) => context.commit("LOAD_DATA", payload)
  },
  getters: {
    GET_DATA: state => state.data
  },
  mutations: {
    LOAD_DATA: (state, payload) => (state.data = payload)
  }
});
