import "../sass/index.scss";
window.axios = require("axios");
import Vue from "vue";
import App from "./components/App.vue";
import store from "./store/store";

function requireAll(r) {
  r.keys().forEach(r);
}
requireAll(require.context("../svg/", true, /\.svg$/));

Vue.component("app", App);

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
